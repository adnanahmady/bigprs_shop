<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BigPrs_Shop
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="ruban ruban-content-overwrite ruban-prefix-border-overwrite">
		<div class="site-info bigprs-footer">
            <?php bigprs_shop_footer_menu () ; ?>
		</div><!-- .site-info -->
        <?php get_sidebar('footer'); ?>
	</footer><!-- #colophon -->
  <!-- </div>.content-feild -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

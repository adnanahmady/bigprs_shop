<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package BigPrs_Shop
 */

if ( ! function_exists( 'bigprs_shop_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function bigprs_shop_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'منتشر شده در %s', 'post date', 'bigprs_shop' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'توسط %s', 'post author', 'bigprs_shop' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'bigprs_shop_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function bigprs_shop_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ' ', 'bigprs_shop' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<div class="cat-links">' . esc_html__( '%2$s دسته ها %3$s %1$s', 'bigprs_shop' ) . '</div>',
                    $categories_list,
                    '<div class="category-title">','</div>'); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ' ', 'list item separator', 'bigprs_shop' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<div class="tags-links">' . esc_html__( '%2$s تگ شده با %3$s %1$s', 'bigprs_shop' ) . '</div>',
                    $tags_list,
                    '<div class="tag-title">',
                    '</div>' ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<div class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'یه نظر بدید <span class="screen-reader-text"> درباره %s</span>', 'bigprs_shop' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</div>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'ویرایش <span class="screen-reader-text">%s</span>', 'bigprs_shop' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<div class="edit-link">',
			'</div>'
		);
	}
endif;

/**
 * create imenu that is on top of the page
 * other locations: 
 * > functions.php
 * > header.php
 * 
 */
function bigprs_shop_imenu() {
	if ( has_nav_menu ( 'imenu' ) ) :
		wp_nav_menu (
			array (
				'theme_location'	=> 'imenu',
				'container'			=> 'div',
				'container_id'		=> 'the_imenu_container',
				'container_class'	=> 'ruban-menu',
				'menu_id'			=> 'the_imenu',
				'menu_class'		=> 'ruban-content-area',
                'link_before'       => '<span class="ruban-menu-link ruban-responsive">',
                'link_after'        => '</span>',
				'depth'				=> 1,
				'fallback_cd'		=> '',
			)
		);
	endif;
}


function bigprs_shop_footer_menu () {
    if ( has_nav_menu ( 'footer-menu' ) ) {
        wp_nav_menu (
            array (
                'theme_location' => 'footer-menu',
                'container' => 'div',
                'container_id' => 'footer-menu',
                'container_class' => 'footer-menu',
                'menu_id' => 'footer-menu-items',
                'menu_class' => 'footer-menu-items',
                'depth' => 3,
                'fallback_cb' => '',
            )
        );
    }
}
<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package BigPrs_Shop
 */

?>
<?php //if ( get_post_type() || 'product' === get_post_type () ) : ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<span class="entry-title h2"><a href="%s" class="entry-title-text" rel="bookmark">', esc_url( get_permalink() ) ), '</a></span>' ); ?>

		<?php if ( 'post' === get_post_type() || 'product' === get_post_type () ) : ?>
            <div class="entry-meta">
                <?php bigprs_shop_posted_on(); ?>
            </div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

    <?php if ( 'post' === get_post_type() || 'product' === get_post_type () ) : ?>
        <footer class="entry-footer">
            <?php bigprs_shop_entry_footer(); ?>
        </footer><!-- .entry-footer -->
    <?php endif;  ?>
</article><!-- #post-<?php the_ID(); ?> -->

<?php //endif;  ?>
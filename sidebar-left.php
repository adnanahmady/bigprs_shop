<?php
/**
 * the left sidebar page
 * 
 */

 if ( ! is_active_sidebar ( 'left-sidebar' ) ) {
	 return;
 }
?>

<!-- <div class='ruban'>
  <nav id="left-widget" class="content-feild aligncenter ruban-content-area"> -->

<nav id="left-widget" class="content-feild aligncenter ruban-content-area">
  <?php dynamic_sidebar( 'left-sidebar' ); ?>
</nav>
  <!-- </nav>
</div> -->
<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BigPrs_Shop
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
  <!-- <div class="content-feild aligncenter"> -->
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bigprs_shop' ); ?></a>

	
 <div class='ruban sticky-bar'>
 <?php 
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
	if ( has_custom_logo() ) {
			echo '<a href="' . esc_url( home_url( '/' ) ) . '" id="site-logo" ><img src="'. esc_url( $logo[0] ) .'" class="sidebar-site-icon"></a>';
	}
 	bigprs_shop_imenu(); ?>
        <button class="bigprs-search-menu-button fa fa-search"></button>
	<?php if ( function_exists ( 'bigprs_shop_woocommerce_header_cart' ) ) {
		bigprs_shop_woocommerce_header_cart();
	}
 ?>
     <div id="bigprs-search-form-toggle" class="bigprs-search-form-toggle">

         <?php
         if ( function_exists ( 'bigprs_settings_search_form' ) ) :
            bigprs_settings_search_form ();
         else :
            get_search_form () ;
         endif ;
         ?>

     </div>
     <script>
         jQuery(document).ready(function($) {
             $('#bigprs-search-form-toggle').slideUp();
             $(".bigprs-search-menu-button").click ( function () {
                 $('#bigprs-search-form-toggle').slideToggle();
             });
         });
     </script>
     <button class="sidebar-toggle fa fa-bars" aria-controls="widget-navigation" aria-expanded="false" id="sidebar-toggle-button"></button>
 </div>
	<header id="masthead" class="site-header content-feild aligncenter">
	<?php if ( get_header_image() && ('blank' == get_header_textcolor()) ) { ?>
		<figure class="header-image">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo bloginfo ( 'title' ) ; ?>" title="<?php echo bloginfo ( 'title' ) ; ?>">
			</a>
		</figure>
	<?php } // End header image check. ?>
	<?php 
		if ( get_header_image() && !('blank' == get_header_textcolor()) ) { 
			echo '<div class="site-branding header-background-image" style="background-image: url(' . get_header_image() . ')">'; 
		} else {
			echo '<div class="site-branding">';
		}
	?>
          <div class="position-bottom-absolute">
            <div class="site-title-box">
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title h2"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
            </div>
          </div>
		</header><!-- .site-branding -->
	</header><!-- #masthead -->
			<nav id="site-navigation" class="main-navigation more-prefix-height">
                <div class="row">
				<button class="menu-toggle fa fa-bars" aria-controls="primary-menu" aria-expanded="false" id="primary-menu-button"></button>
                </div>
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
				?>
			</nav><!-- #site-navigation -->

	<div id="content" class="site-content content-feild aligncenter lettel-bet-slimer">

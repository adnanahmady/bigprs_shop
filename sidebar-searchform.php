<?php

if ( ! defined( 'ABSPATH' ) ) {
exit;
}

?>
<form role="search" method="get" class="bigprs-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label class="screen-reader-text" for="bigprs-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php _e( 'جستجو برای:', 'bigprs_shop' ); ?></label>
    <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'جستجو &hellip;', 'bigprs_shop' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
    <?php $random_id = wp_rand(); ?>
    <select id="<?php echo $random_id ; ?>">
        <option value="not-product"
        <?php
        if ( filter_input( INPUT_GET, 'post_type' ) === null || filter_input( INPUT_GET, 'post_type' ) !== 'product' ) {
            echo ' selected="selected"';
        }
        ?>
        >تمامی سایت</option>
        <option value="product"
            <?php
            if ( filter_input( INPUT_GET, 'post_type' ) !== null && filter_input( INPUT_GET, 'post_type' ) === 'product' ) {
                echo ' selected="selected"';
            }
            ?>
        >محصولات</option>
    </select>
    <button class="search-icon" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
</form>
<script>
    document.getElementById ( '<?php echo $random_id ; ?>' ).onchange = function () {
        var select = document.getElementById('<?php echo $random_id ; ?>').value;
        if (select == 'product') {
            var hidden = document.createElement ( 'INPUT' ) ;
            hidden.setAttribute ( 'type', 'hidden' ) ;
            hidden.setAttribute ( 'name', 'post_type' ) ;
            hidden.setAttribute ( 'value', 'product' ) ;
            hidden.setAttribute ( 'id', 'bigprs-product-type' ) ;
            this.parentElement.appendChild ( hidden ) ;
        } else {
            let hidden = document.getElementById ( 'bigprs-product-type' ) ;
            this.parentElement.removeChild ( hidden ) ;
        }
    }
</script>
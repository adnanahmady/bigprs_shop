<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label>
        <span class="screen-reader-text">جستجو برای:</span>
        <input type="search" class="search-field" placeholder="جستجوی پست …" value="<?php the_search_query() ?>" name="s">
    </label>
    <!--<input type="submit" class="search-submit" value="Search">-->
    <!-- <button type="submit" class="search-icon search-submit">جستجو</button> -->
    <button class="search-icon" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
</form>
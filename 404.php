<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package BigPrs_Shop
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
                <div class="page-content">
                    <header class="page-header prefix">
                        <span class="page-title h2"><?php esc_html_e( 'اووپس, این صفحه پیدا نشد.', 'bigprs_shop' ); ?></span>
                    </header><!-- .page-header -->
                    <p class="not-found-content"><?php esc_html_e( 'به نظر میاد در این مکان چیزی پیدا نشد. شاید بخوای یکی از لینکای زیر رو آزمایش کنی و یا بخوای با جستجو چیزی که میخوای رو پیدا کنی؟', 'bigprs_shop' ); ?></p>
                </div><!-- .page-content -->
					<?php

//					get_search_form();

                    bigprs_settings_search_form();
                    the_widget( 'WP_Widget_Recent_Posts' );
					?>

					<div class="widget widget_categories">
						<span class="widget-title h2 prefix"><?php esc_html_e( 'بیشترین دسته های استفاده شده', 'bigprs_shop' ); ?></span>
						<ul>
						<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						?>
						</ul>
					</div><!-- .widget -->

					<?php

						/* translators: %1$s: smiley */
						$archive_content = '<p class="entry-content not-found-content">' . sprintf( esc_html__( 'یه نگاهی به آرشیو ماهانه بندازید. %1$s', 'bigprs_shop' ), convert_smilies( ':)' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', [
						        "before_title" => "<span class='widget-title prefix h2'>",
                                "after_title" => "</span>$archive_content"
                        ] );

						the_widget( 'WP_Widget_Tag_Cloud', '', [
                            "before_title" => "<span class='widget-title prefix h2'>",
                            "after_title" => "</span>"
                        ]);
					?>

			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();


















